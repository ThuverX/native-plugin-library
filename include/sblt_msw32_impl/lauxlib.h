#pragma once

// Check and Opt functions, from LAuxLib

#define lua_isnumber(L,n)		(lua_type(L, (n)) == LUA_TNUMBER)
#define lua_isstring(L,n)		(lua_type(L, (n)) == LUA_TSTRING || lua_isnumber(L,n))

#define luaL_getmetatable(L,n) (lua_getfield(L, LUA_REGISTRYINDEX, (n)))

#define luaL_argcheck(L, cond,numarg,extramsg)  \
                ((void)((cond) || luaL_argerror(L, (numarg), (extramsg))))
#define luaL_checkstring(L,n)   (luaL_checklstring(L, (n), NULL))
#define luaL_optstring(L,n,d)   (luaL_optlstring(L, (n), (d), NULL))
#define luaL_checkint(L,n)      ((int)luaL_checkinteger(L, (n)))
#define luaL_optint(L,n,d)      ((int)luaL_optinteger(L, (n), (d)))
#define luaL_checklong(L,n)     ((long)luaL_checkinteger(L, (n)))
#define luaL_optlong(L,n,d)     ((long)luaL_optinteger(L, (n), (d)))

#define luaL_opt(L,f,n,d)       (lua_isnoneornil(L,(n)) ? (d) : f(L,(n)))
#define luaL_typename(L,i)      lua_typename(L, lua_type(L,(i)))

#if defined(__cplusplus)
extern "C" {
#endif

int luaL_argerror(lua_State *L, int narg, const char *extramsg);

int luaL_checkoption(lua_State *L, int narg, const char *def, const char *const lst[]);

int luaL_typerror(lua_State *L, int narg, const char *tname);

void luaL_checktype(lua_State *L, int narg, int t);

void luaL_checkany(lua_State *L, int narg);

const char *luaL_checklstring(lua_State *L, int narg, size_t *len);

const char *luaL_optlstring(lua_State *L, int narg,
	const char *def, size_t *len);

lua_Number luaL_checknumber(lua_State *L, int narg);

lua_Number luaL_optnumber(lua_State *L, int narg, lua_Number def);

lua_Integer luaL_checkinteger(lua_State *L, int narg);

lua_Integer luaL_optinteger(lua_State *L, int narg,
	lua_Integer def);

#if defined(__cplusplus)
}
#endif
