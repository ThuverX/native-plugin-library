#pragma once

// Needed for BUFSIZ
#include <stdio.h>

typedef struct luaL_Reg {
	const char *name;
	lua_CFunction func;
} luaL_Reg;

typedef struct luaL_Buffer {
	char *p;			/* current position in buffer */
	int lvl;  /* number of strings in the stack (level) */
	lua_State *L;
	char buffer[LUAL_BUFFERSIZE];
} luaL_Buffer;
