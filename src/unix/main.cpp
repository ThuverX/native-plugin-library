// Include the SuperBLT API
#include "superblt.h"

#include <stdint.h>

// Export this as visible
#define API_CB extern "C" __attribute__ ((visibility ("default")))

// Forward on the appropriate calls
API_CB void SuperBLT_Plugin_Setup() { Plugin_Init(); }
API_CB void SuperBLT_Plugin_Init_State(lua_State *L) { Plugin_Setup_Lua(L); }
API_CB void SuperBLT_Plugin_Update() { Plugin_Update(); }
API_CB void SuperBLT_Plugin_PushLua(lua_State *L) { Plugin_PushLua(L); }

// The revision of the SuperBLT API used. This isn't used for now, but will be in the
// future to ensure backwards-compatibility.
API_CB const uint64_t SBLT_API_REVISION = 1;

// Logging
namespace blt::plugins {
	void superblt_export_unix_logger(const char* message, int level, const char* file, int line);
}

void pd2_log(const char* message, int level, const char* file, int line) {
	blt::plugins::superblt_export_unix_logger(message, level, file, line);
}
