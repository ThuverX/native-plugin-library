# SuperBLT Plugin Library

This library handles the basic functions of a SuperBLT
plugin, such as platform-specific initialization.

For convenience, it's many header files can be compiled into a
single header file. Running `flattenheader.sh` (a GNU BASH script,
so you'll need some way to run BASH if you're using Windows) generates
`build/superblt_flat.sh`, or you can supply a filename to it to put the
file somewhere else.

To use it, compile and link to it (it's a static library),
include and implement the top few functions from it's header file.

If you're just getting started, you should probably use the [plugin template](https://gitlab.com/SuperBLT/native-plugin-template)
(which uses this library, and includes a precompiled copy) or at least look at how it does stuff.

