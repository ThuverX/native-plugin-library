#pragma once

#include <string>
#include <vector>
#include <functional>
#include <cstdint>

enum class HookMode
{
    REPLACER = 0,
    PLAIN_FILE,
    DIRECT_BUNDLE,
};

#define PD2_HOOK_ASSET_FILE(name, ext, replacer) pd2_db_hook_asset_file(name, ext, replacer, "", "", "", (int)HookMode::REPLACER)
#define PD2_HOOK_ASSET_FILE_PLAIN(name, ext, plain_file) pd2_db_hook_asset_file(name, ext, nullptr, plain_file, "", "", (int)HookMode::PLAIN_FILE)
#define PD2_HOOK_ASSET_FILE_DIRECT_BUNDLE(name, ext, direct_bundle_name, direct_bundle_ext) pd2_db_hook_asset_file(name, ext, nullptr, "", direct_bundle_name, direct_bundle_ext, (int)HookMode::DIRECT_BUNDLE)
