// Define the Lua function pointers
#define INIT_FUNC
#include "sblt_msw32_impl/lua_macros.h"
#include "sblt_msw32_impl/misc.h"
#include "sblt_msw32_impl/fptrs.h"

// Include the rest of the SuperBLT API
#include "superblt.h"

typedef void*(*lua_access_func)(const char*);

SBLT_API_EXPORT void SuperBLT_Plugin_Setup(lua_access_func get_exposed_function) {
	// Load all our Lua functions
	for (const auto &func_ptr : all_lua_funcs_list) {
		AutoFuncSetup &func = *func_ptr;
		*func.ptr = get_exposed_function(func.name);
	}

	Plugin_Init();
}

// Forward on the appropriate calls
SBLT_API_EXPORT void SuperBLT_Plugin_Init_State(lua_State *L) { Plugin_Setup_Lua(L); }
SBLT_API_EXPORT void SuperBLT_Plugin_Update() { Plugin_Update(); }
SBLT_API_EXPORT void SuperBLT_Plugin_PushLua(lua_State *L) { Plugin_PushLua(L); }

// The revision of the SuperBLT API used. This isn't used for now, but will be in the
// future to ensure backwards-compatibility.
SBLT_API_EXPORT const uint64_t SBLT_API_REVISION = 1;
