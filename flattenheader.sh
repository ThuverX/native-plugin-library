#!/bin/bash

# Flattens the library headers into a single file
# for ease of depending on it

if [[ "$1" == "--" ]]; then
	OUTFILE=/dev/stdout
elif [[ -n $1 ]]; then
	OUTFILE=$1
else
	OUTFILE=$(dirname "$0")/build/superblt_flat.h
fi

# Convert OUTFILE to an absolute path, so we can
# change directory without breaking anything
OUTFILE="$(realpath --no-symlinks "$OUTFILE")"

# Change to the base directory
cd $(dirname "$0")

# Load the include directory
cd "include"

exclude_platform_regex='//#hp_flatten_os_only ([0-9]+) (.+) //'
include_regex='^\#include "(.*)"$'

ourplatform=unix
if grep -iqE "(Microsoft|WSL|MinGW|MSYS)" /proc/version &> /dev/null ; then
	ourplatform=mswindows
fi

function flattenfile {
	if grep -U $'\x0D' "$1"; then
		echo "ERROR: File $1 contains CRLF newlines!" > /dev/stderr
		exit 1
	fi

	skip_count=0

	while IFS='' read -r li; do 
		if [[ $skip_count -gt 0 ]]; then
			skip_count=$(( $skip_count - 1 ))
			echo "//!/ $li"
			continue
		fi

		if [[ $li =~ $exclude_platform_regex ]]; then
			PLATFORM=${BASH_REMATCH[2]}
			COUNT=${BASH_REMATCH[1]}
			if ! [[ $COUNT =~ ^[0-9]+$ ]]; then
				echo "${#COUNT}" > /dev/stderr
				echo "ERROR: File $1 has non-numerical value '$COUNT' on line '$li'" > /dev/stderr
				exit 1
			fi

			if ! [[ $PLATFORM == $ourplatform ]]; then
				echo "$li"
				echo "// Excluding $COUNT lines due to platform mismatch"
				skip_count=$COUNT
			fi
		elif [[ $li =~ $include_regex ]] ; then
			NEXT=$(dirname "$1")/${BASH_REMATCH[1]}
			echo "// Including $NEXT"
			flattenfile "$NEXT" | sed '/#pragma once/d'
			echo "// Done Including $NEXT"
		else
			echo "$li"
		fi
	done < $1
}

# Start flattening with SuperBLT.h
flattenfile superblt.h > "$OUTFILE"

